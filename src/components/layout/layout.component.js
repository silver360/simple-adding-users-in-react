import React from 'react';

import Footer from '../footer/footer.component';
import Header from '../header/header.component';
import Users from '../users/users.component';

export default class Layout extends React.Component {

    render() {
        return (
            <div>
                <Header />
                <Users />
                <Footer />
            </div>
        );
    }
}