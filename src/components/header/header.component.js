import React from 'react';

//import Image from './header-logo.png';
const src = require('./header-logo.png');

// var img = document.createElement('img');
//     img.src = require('./header-logo.png');

export default class Header extends React.Component {

    render() {
        return (
            <div class="header">
                <img src="{src}" />
                <a class="header__link" href="/">www.positionly.com</a>
            </div>
        );
    }
}
